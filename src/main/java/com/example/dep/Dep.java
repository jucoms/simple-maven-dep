package com.example.dep;

/**
 * Hello name!
 *
 */
public class Dep
{
    public static void hello( String name )
    {
        System.out.println( "Hello Dear" + name + "!" );
        System.out.println( "Your T-factor is N" + "!" );
        System.out.println( "Your last acquired competency is X days old" + "!" );
        System.out.println( "You have acquired XX competencies" + "!" );
        System.out.println( "You are compliant" + "!" );
    }
}
